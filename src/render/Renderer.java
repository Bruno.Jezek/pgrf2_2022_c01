package render;

import model.Part;
import model.Scene;
import model.Solid;
import model.Vertex;
import transforms.Mat4;
import transforms.Mat4Identity;

public class Renderer {

    private Mat4 transformMatrix = new Mat4Identity();

    private Rasterizer rasterizer;

    public Renderer(Rasterizer rasterizer) {
        this.rasterizer = rasterizer;
    }

    public void setRasterizer(Rasterizer rasterizer) {
        this.rasterizer = rasterizer;
    }

    public void render(Solid solid){
        for (Part part:solid.getPartBuffer()){
            switch (part.getTopology()){
                case POINTS:
                    //TODO
                    break;
                case TRIANGLES:
                    for(int i= 0; i<part.getCount();  i++ ) {
                        int idxA = solid.getIndexBuffer().get(part.getStart() + i*3);
                        int idxB = solid.getIndexBuffer().get(part.getStart() + i*3 + 1);
                        int idxC = solid.getIndexBuffer().get(part.getStart() + i*3 + 2);
                        Vertex vA = solid.getVertexBuffer().get(idxA);
                        Vertex vB = solid.getVertexBuffer().get(idxB);
                        Vertex vC = solid.getVertexBuffer().get(idxC);
                        renderTriangle(vA, vB, vC);
                    }
                    break;
                case LINES:
                    break;
            }
        }
    }
    public void render(Scene scene){
        for (Solid solid:scene.getSolids()) {
            render(solid);
        }
    }
    private void renderLine(Vertex a, Vertex b){

    }

    private void renderTriangle(Vertex a, Vertex b, Vertex c){
        rasterizer.rasterizeTriangle(a, b, c);
        renderPoint(a);
        renderPoint(b);
        renderPoint(c);
        //aplikovat transformace
        a = a.mul(transformMatrix);
        //TODO ...

        //clip
        //fast clip
        //TODO

        //orezani na podminku z>=0
        //usporadat podle z
        //TODO a.z >= b.z >= c.z

        if (a.getPosition().getZ()<0)
            return;
        if (b.getPosition().getZ()<0){
            double s1 = (0 - b.getPosition().getZ()) / (a.getPosition().getZ() - b.getPosition().getZ());
            Vertex ab = b.mul(1-s1).add(a.mul(s1));
            Vertex ac = a; //TODO
            //rasterizer.rasterizeTriangle(a, ab, ac);
            rasterizer.rasterizePoint(a);
            rasterizer.rasterizePoint(ab);
            rasterizer.rasterizePoint(ac);
        }
        //TODO zbytek orezani



    }
    private void renderPoint(Vertex v){
    //podminky orezani
        rasterizer.rasterizePoint(v);
    }

}
