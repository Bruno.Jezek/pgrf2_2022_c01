package render;

import model.Vertex;

@FunctionalInterface
public interface FuncShader {
    int shade(Vertex v);
}
