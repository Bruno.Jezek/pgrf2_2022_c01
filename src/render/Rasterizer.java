package render;

import model.Vertex;
import raster.Visibility;
import transforms.Vec2D;

public class Rasterizer {
    private Visibility visibility;
    private FuncShader shader = new FuncShader(){

        @Override
        public int shade(Vertex v) {
            return 0xFFFFFF;
        }
    };

    public Rasterizer(Visibility visibility) {
        this.visibility = visibility;
    }

    public void setShader(FuncShader shader) {
        this.shader = shader;
    }

    public void rasterizeTriangle(Vertex a, Vertex b, Vertex c){
        //dehomogenizace

        a = a.mul(1/a.getPosition().getW());

        //viewport
        int w = visibility.getImg().getWidth();
        int h = visibility.getImg().getHeight();
        Vec2D aVP = new Vec2D(a.getPosition().getX(),-1 * a.getPosition().getY());
        aVP = aVP.add(new Vec2D(1,1));
        aVP = aVP.mul(new Vec2D((w-1)/2, (h-1)/2));
        Vec2D bVP = new Vec2D(b.getPosition().getX(),-1 * b.getPosition().getY());
        bVP = bVP.add(new Vec2D(1,1));
        bVP = bVP.mul(new Vec2D((w-1)/2, (h-1)/2));
        Vec2D cVP = new Vec2D(c.getPosition().getX(),-1 * c.getPosition().getY());
        cVP = cVP.add(new Vec2D(1,1));
        cVP = cVP.mul(new Vec2D((w-1)/2, (h-1)/2));

        visibility.getImg().getGraphics().drawLine((int)aVP.getX(),(int)aVP.getY(),(int)bVP.getX(),(int)bVP.getY());

        //TODO usporadat vrcholy a,b,c podle aVP.y <= bVP.y <= cVP.y

        //rasterize horni polovinu trojuhelnika od aVP.y do bVP.y

        //visibility.setVisiblePixelWithZtest(
        //        (int) aVP.getX(), (int) aVP.getY(), a.getPosition().getZ(), abc.getColor());
        //rasterize dolni polovinu trojuhelnika od bVP.y do cVP.y

    }
    public void rasterizePoint(Vertex a){
        //dehomogenizace

        a = a.mul(1/a.getPosition().getW());

        //viewport
        int w = visibility.getImg().getWidth();
        int h = visibility.getImg().getHeight();
        Vec2D aVP = new Vec2D(a.getPosition().getX(),-1 * a.getPosition().getY());
        aVP = aVP.add(new Vec2D(1,1));
        aVP = aVP.mul(new Vec2D((w-1)/2, (h-1)/2));
        //aVP = aVP otocit y, posunout 0,0, zvetsit na w,h

        visibility.setVisiblePixelWithZtest(
                (int) aVP.getX(), (int) aVP.getY(), a.getPosition().getZ(), shader.shade(a));

    }
}
