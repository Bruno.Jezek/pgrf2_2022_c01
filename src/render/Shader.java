package render;

import model.Vertex;

public class Shader {

    public int shade(Vertex v){
        return v.getColor();

    }
}
