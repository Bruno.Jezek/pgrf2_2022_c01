package solids;

import model.Part;
import model.Solid;
import model.Vertex;
import transforms.Point3D;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Cube extends Solid {
    public Cube(){
        super();
        vertexBuffer.add(new Vertex(new Point3D(-.9,-.9,0.5)));
        vertexBuffer.add(new Vertex(new Point3D(.9,0,0.5)));
        vertexBuffer.add(new Vertex(new Point3D(0,.9,0.5)));
        indexBuffer.addAll(Arrays.asList(0,1,2));
        partBuffer.add(new Part(0, 1, Part.Topology.TRIANGLES));
        partBuffer.add(new Part(1, 1, Part.Topology.LINES));
    }
}
