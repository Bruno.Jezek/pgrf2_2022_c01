package control;

import model.Solid;
import model.Vertex;
import raster.RasterBufferImage;
import raster.Visibility;
import render.FuncShader;
import render.Rasterizer;
import render.Renderer;
import render.Shader;
import solids.Cube;
import view.Panel;

import java.awt.*;
import java.awt.event.*;

public class Controller3D implements Controller {

    private final Panel panel;

    private int width,height;
    private boolean pressed = false;
    private int ox, oy;
    private Visibility visibility;
    private Renderer renderer;
    private Rasterizer rasterizer;



    public Controller3D(Panel panel) {
        this.panel = panel;
        initObjects(panel.getRaster());
        initListeners(panel);
        redraw();
    }

    public void initObjects(RasterBufferImage raster) {
        raster.setClearValue(0x101010);
        visibility = new Visibility(raster);
        rasterizer = new Rasterizer(visibility);
        renderer = new Renderer(rasterizer);
    }

    @Override
    public void initListeners(Panel panel) {
        panel.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent ev) {
                if (ev.getButton() == MouseEvent.BUTTON1) {
                    pressed = true;
                    ox = ev.getX();
                    oy = ev.getY();
                    panel.getRaster().setPixel(ox,oy,0xff0000);
                    redraw();
                }
            }

            public void mouseReleased(MouseEvent ev) {
                if (ev.getButton() == MouseEvent.BUTTON1) {
                    panel.getRaster().setPixel(ox,oy,0xffff);
                    pressed = false;
                    redraw();
                }
            }
        });

        panel.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent ev) {
                if (pressed) {
                    ox=ev.getX();
                    oy=ev.getY();
                    panel.getRaster().setPixel(ox,oy,0xffff00);
                    redraw();

                }
            }
        });

        panel.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent key) {
                // out.print(key.getKeyCode());
                switch (key.getKeyCode()) {
                    case KeyEvent.VK_BACK_SPACE:
                        panel.clear();
                        initObjects(panel.getRaster());
                        break;
                    case KeyEvent.VK_M:
                        break;
                }
                redraw();
            }
        });

        panel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                panel.resize();
                initObjects(panel.getRaster());
                redraw();
            }
        });
    }

    private void redraw() {
        visibility.clear();
        width = panel.getRaster().getWidth();
        height = panel.getRaster().getHeight();
        Graphics g = panel.getRaster().getImg().getGraphics();

        g.drawLine(0,0, panel.getWidth(), panel.getHeight());
        g.drawString(panel.getRaster().getClass().getName(),10,30);
        g.drawString("UHK FIM PGRF",width-150,height-10);

        Solid test = new Cube();

        rasterizer.setShader(new FuncShader(){
            @Override
            public int shade(Vertex v){
                return 0xffff00;

            }
        });

        rasterizer.setShader((Vertex v)->{return 0xff;});
        rasterizer.setShader(v -> v.getColor());
        renderer.render(test);
        panel.repaint();
    }

}

