package raster;

import javafx.util.Pair;

import java.util.HashMap;
import java.util.List;

public class RasterDepthBuffer implements Raster<Double>{
    //HashMap<Pair<Integer,Integer>, Double>
    //private double[][] array;
//    private List<Double> list;
    private int width, height;
    private double[] array;

    public RasterDepthBuffer(int width, int height) {
        this.width = width;
        this.height = height;
        this.array = new double[width * height];
    }

    @Override
    public void setPixel(int x, int y, Double depth) {
//        array[x][y] = depth.doubleValue();
        if (checkOutOfBounds(x,y)) return;
        int index = x + y*getWidth();
//        list.set(index,depth);
        array[index] = depth.doubleValue();
    }

    private boolean checkOutOfBounds(int x, int y) {
        return false; //TODO
    }

    @Override
    public Double getPixel(int x, int y) {
        if (checkOutOfBounds(x,y)) return null;
        int index = x + y*getWidth();
       return new Double(array[index]);
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public void clear() {

    }
}
