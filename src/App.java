import control.Controller3D;
import model.Solid;
import raster.RasterDepthBuffer;
import raster.Visibility;
import solids.Cube;
import view.*;

import javax.swing.*;

public class App {
    public static void main(String[] args) {
        /*System.out.println("test raster");
        RasterDepthBuffer db = new RasterDepthBuffer(21, 21);
        db.setPixel(10,10, 0.5);
        db.setPixel(10,10, 0.7);
        System.out.println(db.getPixel(10,10));
        db.setPixel(10,100, 0.3);
        System.out.println(db.getPixel(10,1) );
        System.out.println(db.getPixel(10,100) );
        System.out.println(db.getPixel(10,10));

        System.out.println("test visibility - color");
        Visibility vis = new Visibility(21,21);
        vis.setVisiblePixel(20,20, 10);
        System.out.println(vis.getColorOfVisiblePixel(20,20) & 0xffffff);
        vis.setVisiblePixel(20,20, 20);
        System.out.println(vis.getColorOfVisiblePixel(20,20) & 0xffffff);

        System.out.println("test visibility - depth");
        vis.setVisiblePixelWithZtest(20,20,0.5, 10);
        System.out.println(vis.getColorOfVisiblePixel(20,20) & 0xffffff);
        System.out.println(vis.getDepthOfVisiblePixel(20,20));
        vis.setVisiblePixelWithZtest(20,20,0.7, 20);
        System.out.println(vis.getColorOfVisiblePixel(20,20) & 0xffffff);
        System.out.println(vis.getDepthOfVisiblePixel(20,20));
        vis.setVisiblePixelWithZtest(20,20,0.3, 30);
        System.out.println(vis.getColorOfVisiblePixel(20,20) & 0xffffff);
        System.out.println(vis.getDepthOfVisiblePixel(20,20));

        //new Solid();
        Cube cube = new Cube();
        System.out.println(cube);*/

        SwingUtilities.invokeLater(() -> {
            Window window = new Window();
            new Controller3D(window.getPanel());
            window.setVisible(true);
        });
    }

}
