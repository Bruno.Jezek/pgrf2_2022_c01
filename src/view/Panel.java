package view;

import raster.RasterBufferImage;

import javax.swing.*;
import java.awt.*;

public class Panel extends JPanel {

    private RasterBufferImage raster;

    public RasterBufferImage getRaster() {
        return raster;
    }

    public static final int WIDTH = 800, HEIGHT = 600;

    Panel() {
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        raster = new RasterBufferImage(WIDTH, HEIGHT);
        raster.setClearValue(Color.BLACK.getRGB());
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        raster.repaint(g);
    }

    public void resize(){
        if (this.getWidth()<1 || this.getHeight()<1)
            return;
        if (this.getWidth()<=raster.getWidth() && this.getHeight()<=raster.getHeight()) //no resize if new is smaller
            return;
        RasterBufferImage newRaster = new RasterBufferImage(this.getWidth(), this.getHeight());

        newRaster.draw(raster);
        raster = newRaster;
    }

    public void clear() {
        raster.clear();
    }
}
