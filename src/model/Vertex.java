package model;

import transforms.Mat4;
import transforms.Point3D;

public class Vertex implements Vectorizable<Vertex>{
    private Point3D position;
    private int color;
    //TODO one

    public Point3D getPosition() {
        return position;
    }

    public void setPosition(Point3D position) {
        this.position = position;
    }

    public Vertex(Point3D position) {
        this.position = position;
        this.color = 0xff00ff;
    }

    public Vertex add(Vertex v){
        //TODO
        return this;
    }

    public Vertex mul(double k){
        //TODO
        return this;
    }

    public Vertex mul(Mat4 transform){
        return new Vertex(position.mul(transform));
    }

    public int getColor() {
        return color;
    }
}
