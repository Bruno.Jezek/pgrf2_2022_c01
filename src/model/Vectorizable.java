package model;

public interface Vectorizable<V> {
    V add(V v);
    V mul(double value);
}
