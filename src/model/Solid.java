package model;

import java.util.ArrayList;
import java.util.List;

public abstract class Solid {
    protected List<Vertex> vertexBuffer = new ArrayList<>();
    protected List<Integer> indexBuffer = new ArrayList<>();
    protected List<Part> partBuffer = new ArrayList<>();

    public Solid(){

    }

    public List<Vertex> getVertexBuffer() {
        return vertexBuffer;
    }

    public List<Integer> getIndexBuffer() {
        return indexBuffer;
    }

    public List<Part> getPartBuffer() {
        return partBuffer;
    }


    @Override
    public String toString() {
        return "Solid{" +
                "vertexBuffer=" + vertexBuffer +
                ", indexBuffer=" + indexBuffer +
                ", partBuffer=" + partBuffer +
                '}';
    }
}
